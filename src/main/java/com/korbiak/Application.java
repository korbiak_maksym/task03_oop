package com.korbiak;

import view.MyView;
/**
 * Application.
 *
 * @author Maksym Korbiak
 */
public final class  Application {
    /**
     *Utility classes should not have a public or default constructor.
     */
    private Application() {
    }

    /**
     * Main method.
     * @param args args
     */
    public static void main(final String[] args) {
        MyView myView = new MyView();
        myView.show();
    }
}
