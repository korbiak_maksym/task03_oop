
package controller;

import model.Stone;

import java.util.ArrayList;

/**
 * Interface Controller.
 *
 * @author Maksym Korbiak
 */
public interface Controller {
    /**
     * Return stones in necklace.
     *
     * @return list of stones.
     */
    ArrayList<Stone> getList();

    /**
     * Adds a stone to the necklace.
     *
     * @param id           id of stone.
     * @param weight       weight of stone.
     * @param cost         cost of stone.
     * @param transparency transparency of stone.
     */
    void setNewStone(int id, double weight, double cost, double transparency);

    /**
     * Sorting necklace stones by price.
     */
    void sort();

    /**
     * Transparency Range.
     *
     * @param from from tran.
     * @param to   to tran.
     * @return New list of stones.
     */
    ArrayList<Stone> getTransparencyRange(double from, double to);

    /**
     * Amount of weight and cost.
     *
     * @return Amount of weight and cost.
     */
    double[] getAmount();
}
