
package controller;

import model.BusinessLogic;
import model.Model;
import model.Stone;

import java.util.ArrayList;

/**
 * ControllerImpl.
 *
 * @author Maksym Korbiak
 */
public class ControllerImpl implements Controller {
    /**
     * Creation object of Model.
     */
    private Model model;

    /**
     * Initialization model.
     */
    public ControllerImpl() {
        this.model = new BusinessLogic();
    }

    /**
     * Implements method in Controller.
     *
     * @return new list of stones.
     */
    @Override
    public final ArrayList<Stone> getList() {
        return model.getList();
    }

    /**
     * Implements method in Controller.
     *
     * @param id           id of stone.
     * @param weight       weight of stone.
     * @param cost         cost of stone.
     * @param transparency transparency of stone.
     */
    @Override
    public final void setNewStone(
            final int id, final double weight,
            final double cost, final double transparency) {
        model.setNewStone(id, weight, cost, transparency);
    }

    /**
     * Implements method in Controller.
     */
    @Override
    public final void sort() {
        model.sort();
    }

    /**
     * Implements method in Controller.
     *
     * @param from from tran.
     * @param to   to tran.
     * @return list.
     */
    @Override
    public final ArrayList<Stone> getTransparencyRange(final double from,
                                                       final double to) {
        return model.getTransparencyRange(from, to);
    }

    /**
     * Implements method in Controller.
     *
     * @return array of amount.
     */
    @Override
    public final double[] getAmount() {
        double[] amount = new double[2];
        amount[0] = model.getAmountWeight();
        amount[1] = model.getAmountCost();
        return amount;
    }
}
