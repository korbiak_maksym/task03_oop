package model.gs;

/**
 * Class Diamond.
 *
 * @author Maksym Korbiak
 */
public class Diamond extends GemStone {

    /**
     * Initialization.
     *
     * @param weight       Weight of stone.
     * @param cost         Cost of stone.
     * @param transparency Transparency of stone.
     */
    public Diamond(
            final double weight,
            final double cost, final double transparency) {
        super(weight, cost, transparency);
    }

    /**
     * Override toString.
     *
     * @return String.
     */
    @Override
    public final String toString() {
        return "Diamond{"
                + getWeight()
                + ","
                + getCost()
                + ","
                + getTransparency()
                + "}";
    }
}
