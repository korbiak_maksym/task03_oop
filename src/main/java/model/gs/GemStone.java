package model.gs;

import model.Stone;
/**
 * Class GemStone.
 *
 * @author Maksym Korbiak
 */
public abstract class GemStone extends Stone {
    /**
     * Initialization.
     * @param weight       Weight of stone.
     * @param cost         Cost of stone.
     * @param transparency Transparency of stone.
     */
    public GemStone(
            final double weight,
            final double cost, final double transparency) {
        super(weight, cost, transparency);
    }
}
