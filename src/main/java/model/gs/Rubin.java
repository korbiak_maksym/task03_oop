package model.gs;

/**
 * Class Rubin.
 *
 * @author Maksym Korbiak
 */

public class Rubin extends GemStone {
    /**
     * Initialization.
     *
     * @param weight       Weight of stone.
     * @param cost         Cost of stone.
     * @param transparency Transparency of stone.
     */
    public Rubin(
            final double weight,
            final double cost, final double transparency) {
        super(weight, cost, transparency);
    }

    /**
     * Override toString.
     *
     * @return String.
     */
    @Override
    public final String toString() {

        return "Rubin{"
                + getWeight()
                + ","
                + getCost()
                + ","
                + getTransparency()
                + "}";
    }
}
