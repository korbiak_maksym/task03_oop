package model.gs;

/**
 * Class Sapphire.
 *
 * @author Maksym Korbiak
 */
public class Sapphire extends GemStone {
    /**
     * Initialization.
     *
     * @param weight       Weight of stone.
     * @param cost         Cost of stone.
     * @param transparency Transparency of stone.
     */
    public Sapphire(
            final double weight,
            final double cost, final double transparency) {
        super(weight, cost, transparency);
    }

    /**
     * Override toString.
     *
     * @return String.
     */
    @Override
    public final String toString() {

        return "Sapphire{"
                + getWeight()
                + ","
                + getCost()
                + ","
                + getTransparency()
                + "}";
    }
}
