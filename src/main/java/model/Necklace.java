package model;

import model.gs.Diamond;
import model.gs.Rubin;
import model.gs.Sapphire;
import model.sps.Amethyst;
import model.sps.Onyx;
import model.sps.Topaz;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Class Necklace.
 *
 * @author Maksym Korbiak
 */
public class Necklace {
    /**
     * Creation object of List<>.
     */
    private List<Stone> list;

    /**
     * Init. object of List.
     */
    public Necklace() {
        this.list = new ArrayList<Stone>();
    }

    /**
     * Get list of stones.
     *
     * @return list
     */
    public final ArrayList<Stone> getList() {
        return (ArrayList<Stone>) list;
    }

    /**
     * Adds a stone to the necklace.
     *
     * @param id           id of stone.
     * @param weight       weight of stone.
     * @param cost         cost of stone.
     * @param transparency transparency of stone.
     */
    public final void setNewStone(
            final int id, final double weight,
            final double cost, final double transparency) {
        Stone stone;

        switch (id) {
            case 1:
                stone = new Diamond(weight, cost, transparency);
                break;
            case 2:
                stone = new Rubin(weight, cost, transparency);
                break;
            case 3:
                stone = new Sapphire(weight, cost, transparency) {
                };
                break;
            case 21:
                stone = new Amethyst(weight, cost, transparency);
                break;
            case 22:
                stone = new Onyx(weight, cost, transparency);
                break;
            case 23:
                stone = new Topaz(weight, cost, transparency);
                break;
            default:
                stone = null;
        }
        list.add(stone);
    }

    /**
     * Sorting necklace stones by price.
     */
    public final void sort() {
        list.sort(Comparator.comparing(Stone::getCost));
    }

    /**
     * Transparency Range.
     *
     * @param from from tran.
     * @param to   to tran.
     * @return New list of stones.
     */
    public final ArrayList<Stone> getRangeList(
            final double from, final double to) {
        List<Stone> rangeList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getTransparency()
                    > from && list.get(i).getTransparency() < to) {
                rangeList.add(list.get(i));
            }
        }
        return (ArrayList<Stone>) rangeList;
    }

    /**
     * Get amount of weight.
     *
     * @return amount of weight
     */
    public final double getAmountCost() {
        double amount = 0;
        for (Stone i : list) {
            amount += i.getCost();
        }
        return amount;
    }

    /**
     * Get amount of cost.
     *
     * @return amount of cost.
     */
    public final double getAmountWeight() {
        double amount = 0;
        for (Stone i : list) {
            amount += i.getWeight();
        }
        return amount;
    }

}
