package model;

/**
 * Class Stone.
 *
 * @author Maksym Korbiak
 */
public abstract class Stone {
    /**
     * Weight of stone.
     */
    private double weight;
    /**
     * Cost of stone.
     */
    private double cost;
    /**
     * Transparency of stone.
     */
    private double transparency;

    /**
     * Initialization.
     *
     * @param weight       Weight of stone.
     * @param cost         Cost of stone.
     * @param transparency Transparency of stone.
     */
    public Stone(
            final double weight,
            final double cost, final double transparency) {
        this.weight = weight;
        this.cost = cost;
        this.transparency = transparency;
    }

    /**
     * Getter.
     *
     * @return Weight.
     */
    public final double getWeight() {
        return weight;
    }

    /**
     * Setter.
     *
     * @param weight Weight of stone.
     */
    public final void setWeight(final double weight) {
        this.weight = weight;
    }

    /**
     * Getter.
     *
     * @return Cost.
     */
    public final double getCost() {
        return cost;
    }

    /**
     * Setter.
     *
     * @param cost Cost of stone.
     */
    public final void setCost(final double cost) {
        this.cost = cost;
    }

    /**
     * Getter.
     *
     * @return Transparency.
     */
    public final double getTransparency() {
        return transparency;
    }

    /**
     * Setter.
     *
     * @param transparency Transparency of stone.
     */
    public final void setTransparency(final double transparency) {
        this.transparency = transparency;
    }

    /**
     * Override toString.
     *
     * @return String.
     */
    @Override
    public String toString() {
        return "Stone{"
                + "weight="
                + weight
                + ", cost="
                + cost
                + ", transparency="
                + transparency
                + '}';
    }
}
