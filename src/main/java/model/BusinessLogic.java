
package model;

import java.util.ArrayList;

/**
 * Interface Model.
 *
 * @author Maksym Korbiak
 */
public class BusinessLogic implements Model {
    /**
     * Creation object of necklace.
     */
    private Necklace necklace;

    /**
     * Iinit. necklace.
     */
    public BusinessLogic() {
        this.necklace = new Necklace();
    }

    /**
     * Get list of stones.
     *
     * @return list of stones.
     */
    public final ArrayList<Stone> getList() {
        return necklace.getList();
    }

    /**
     * Add new stone.
     *
     * @param id           id of stone.
     * @param weight       weight of stone.
     * @param cost         cost of stone.
     * @param transparency transparency of stone.
     */
    public final void setNewStone(
            final int id, final double weight,
            final double cost, final double transparency) {
        necklace.setNewStone(id, weight, cost, transparency);
    }

    /**
     * Sort list.
     */
    public final void sort() {
        necklace.sort();
    }

    /**
     * Get new list.
     *
     * @param from from tran.
     * @param to   to tran.
     * @return ArrayList.
     */
    public final ArrayList<Stone> getTransparencyRange(
            final double from, final double to) {
        return necklace.getRangeList(from, to);
    }

    /**
     * Get amount of weight.
     *
     * @return amount of weight
     */
    @Override
    public final double getAmountWeight() {
        return necklace.getAmountWeight();
    }

    /**
     * Get amount of cost.
     *
     * @return amount of cost
     */
    @Override
    public final double getAmountCost() {
        return necklace.getAmountCost();
    }

}
