package model;

import java.util.ArrayList;

/**
 * Interface Model.
 *
 * @author Maksym Korbiak
 */
public interface Model {
    /**
     * Return stones in necklace.
     *
     * @return list of stones.
     */
    ArrayList<Stone> getList();

    /**
     * Add a stone to the necklace.
     *
     * @param id           id of stone.
     * @param weight       weight of stone.
     * @param cost         cost of stone.
     * @param transparency transparency of stone.
     */
    void setNewStone(int id, double weight, double cost, double transparency);

    /**
     * Sorting necklace stones by price.
     */
    void sort();

    /**
     * Transparency Range.
     *
     * @param from from tran.
     * @param to   to tran.
     * @return New list of stones.
     */
    ArrayList<Stone> getTransparencyRange(double from, double to);

    /**
     * Get amount of weight.
     *
     * @return amount of weight
     */
    double getAmountWeight();

    /**
     * Get amount of cost.
     *
     * @return amount of cost.
     */
    double getAmountCost();
}
