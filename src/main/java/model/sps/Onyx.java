package model.sps;

/**
 * Class Onyx.
 *
 * @author Maksym Korbiak
 */
public class Onyx extends SemiPreciousStone {
    /**
     * Initialization.
     *
     * @param weight       Weight of stone.
     * @param cost         Cost of stone.
     * @param transparency Transparency of stone.
     */
    public Onyx(
            final double weight,
            final double cost, final double transparency) {
        super(weight, cost, transparency);
    }

    /**
     * Override toString.
     *
     * @return String.
     */
    @Override
    public final String toString() {

        return "Onyx{"
                + getWeight()
                + ","
                + getCost()
                + ","
                + getTransparency()
                + "}";
    }
}
