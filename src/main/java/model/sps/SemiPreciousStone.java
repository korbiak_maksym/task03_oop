package model.sps;

import model.Stone;

/**
 * Class SemiPreciousStone.
 *
 * @author Maksym Korbiak
 */
public abstract class SemiPreciousStone extends Stone {
    /**
     * Initialization.
     *
     * @param weight       Weight of stone.
     * @param cost         Cost of stone.
     * @param transparency Transparency of stone.
     */
    public SemiPreciousStone(
            final double weight,
            final double cost,
            final double transparency) {
        super(weight, cost, transparency);
    }
}
