package model.sps;

/**
 * Class Amethyst.
 *
 * @author Maksym Korbiak
 */
public class Amethyst extends SemiPreciousStone {
    /**
     * Initialization.
     *
     * @param weight       Weight of stone.
     * @param cost         Cost of stone.
     * @param transparency Transparency of stone.
     */
    public Amethyst(
            final double weight,
            final double cost, final double transparency) {
        super(weight, cost, transparency);
    }

    /**
     * Override toString.
     *
     * @return String.
     */
    @Override
    public final String toString() {
        return "Amethyst{"
                + getWeight()
                + ","
                + getCost()
                + ","
                + getTransparency()
                + "}";
    }
}
