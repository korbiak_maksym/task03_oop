package model.sps;

/**
 * Class Topaz.
 *
 * @author Maksym Korbiak
 */
public class Topaz extends SemiPreciousStone {
    /**
     * Initialization.
     *
     * @param weight       Weight of stone.
     * @param cost         Cost of stone.
     * @param transparency Transparency of stone.
     */
    public Topaz(
            final double weight,
            final double cost,
            final double transparency) {
        super(weight, cost, transparency);
    }

    /**
     * Override toString.
     *
     * @return String.
     */
    @Override
    public final String toString() {

        return "Topaz{"
                + getWeight()
                + ","
                + getCost()
                + ","
                + getTransparency()
                + "}";
    }
}
