package view;

import controller.Controller;
import controller.ControllerImpl;

import java.util.Scanner;

/**
 * MyView.
 *
 * @author Maksym Korbiak
 */
public class MyView {
    /**
     * Creation scanner to enter data.
     */
    private Scanner scanner;
    /**
     * Creation controller.
     */
    private Controller controller;

    /**
     * Initialization scanner and controller.
     */
    public MyView() {
        scanner = new Scanner(System.in);
        controller = new ControllerImpl();
    }

    /**
     * Main menu.
     */
    public final void show() {
        int k = 0;
        do {
            System.out.println("-------------------------------------");
            System.out.println("1- Show necklace");
            System.out.println("2- Add new stone");
            System.out.println("3- Sort");
            System.out.println("4- Show amount");
            System.out.println("5- Set the range of transparency options");
            System.out.println("6- Exit");
            System.out.println("Your answer:");
            int answer = scanner.nextInt();
            if (answer == 1) {
                pressButton1();
            } else if (answer == 2) {
                pressButton2();
            } else if (answer == 3) {
                pressButton3();
            } else if (answer == 4) {
                pressButton4();
            } else if (answer == 5) {
                pressButton5();
            } else if (answer == 6) {
                k = 1;
            }
        } while (k == 0);
    }

    /**
     * Print List.
     */
    private void pressButton1() {
        System.out.println(controller.getList());
    }

    /**
     * Add new stone to necklace.
     */
    private void pressButton2() {
        System.out.println("-------------------------------------------------");
        System.out.println("Chose stone:");
        double weight = 0;
        double cost = 0;
        double transparency = 0;
        System.out.println("1- Diamond");
        System.out.println("2- Rubin");
        System.out.println("3- Sapphire");
        System.out.println("21- Amethyst");
        System.out.println("22- Onyx");
        System.out.println("23- Topaz");
        System.out.println("Your answer:");
        int answer = scanner.nextInt();
        System.out.println("Weight:");
        weight = scanner.nextDouble();
        System.out.println("Cost:");
        cost = scanner.nextDouble();
        System.out.println("Transparency:");
        transparency = scanner.nextDouble();

        controller.setNewStone(answer, weight, cost, transparency);
    }

    /**
     * Print sorting list by price.
     */
    private void pressButton3() {
        controller.sort();
    }

    /**
     * Print amount of weight and cost.
     */
    private void pressButton4() {
        System.out.println("Weight:"
                + controller.getAmount()[0]
                + "\n"
                + "Cost:"
                + controller.getAmount()[1]);
    }

    /**
     * Print transparency range.
     */
    private void pressButton5() {
        double from = 0;
        double to = 0;

        System.out.println("Set for:");
        from = scanner.nextDouble();
        System.out.println("Set to:");
        to = scanner.nextDouble();
        System.out.println(controller.getTransparencyRange(from, to));
    }

}
